import {Component} from "react";
import axios from "axios";

export default  class CustomerStatus extends Component{
    constructor(props){
        super(props)
        this.state = {
            status:""
        }
    }

    componentDidMount() {
        axios
            .get("http://localhost:8080/api/customer/status")
            .then(response => {
                // create an array of contacts only with relevant data
                console.log(response);
                const newStatus = response.data;
                this.setState({status: newStatus})
                console.log(newStatus);
            })

            .catch(function (error) {
                console.log(error);
            });
    }

    render(){
        return (
            <div>
                <h1>Status - {this.state.status}</h1>
            </div>)
    }
}