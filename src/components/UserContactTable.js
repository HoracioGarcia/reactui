import {Component} from "react";
import axios from "axios";

export default  class UserContactTable extends Component{
    constructor(props){
        super(props)
        this.state = {
            contactTable:[],
            count:0
        }
    }

    componentDidMount() {
        axios
            .get("https://jsonplaceholder.typicode.com/users")
            .then(response => {
                // create an array of contacts only with relevant data
                console.log(response);
                const newContacts = response.data.map(c => {
                    return {
                        id: c.id,
                        name: c.name,
                        email: c.email
                    };
                });
                this.setState({contactTable: newContacts})
                console.log(newContacts);
            })

            .catch(function (error) {
                console.log(error);
            });
    }

    render(){
        return (
            <div>
                <h1>Contact User Table - {this.props.name}</h1>
                <table>
                    {this.state.contactTable.map(d => (
                        <tr>
                            <td>{d.id} </td>
                            <td>{d.name} </td>
                            <td>{d.email} </td>
                        </tr>
                    ))}
                </table>
            </div>)

    }
}