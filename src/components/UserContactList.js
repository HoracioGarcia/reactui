import {Component} from "react";
import axios from "axios";

export default  class UserContactList extends Component{
    constructor(props){
        super(props)
        this.state = {
            contactList:[],
            count:0
        }

    }
    //https://jsonplaceholder.typicode.com/users
    componentDidMount() {
        axios
            .get("https://jsonplaceholder.typicode.com/users")
            .then(response => {
                // create an array of contacts only with relevant data
                console.log(response);
                const newContacts = response.data.map(c => {
                    return {
                        id: c.id,
                        name: c.name,
                        email: c.email
                    };
                });
                this.setState({contactList: newContacts})
                console.log(newContacts);
            })

            .catch(function (error) {
                console.log(error);
            });
    }

    render(){
        return (
            <div>
                <h1>Contact User List - {this.props.name}</h1>
                <ul>
                    {this.state.contactList.map(d => (<li key={d.id}>{d.name}</li>))}
                </ul>
            </div>)

    }
}