import React, {Component} from 'react';
import axios from "axios";

export default class AddCustomer extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeLine1 = this.onChangeLine1.bind(this);
        this.onChangeId = this.onChangeId.bind(this);
        this.saveCustomer = this.saveCustomer.bind(this);

        this.state = {
            id: 0,
            name: "",
            line1:"",
            submitted: false
        };
    }
    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }
    onChangeLine1(e) {
        this.setState({
            line1: e.target.value
        });
    }
    onChangeId(e) {
        this.setState({
            id: e.target.value
        });
    }
    saveCustomer() {
        var data = {
            id: this.state.id,
            name: this.state.name  ,
            address:{line1:this.state.line1}}


        axios.post('http://localhost:8080/api/customer/save', data)
            .then(response => this.setState({ submitted: response.data.id }))
            .catch(e => {
                alert(e);
                console.log(e);
            });
    }

    render() {
        return (<form>
            <h1>Add Customer</h1>
            <div className="form-group">
                <label htmlFor="txtname">   Name : </label>
                <input type="text" className="form-control" id="txtname" aria-describedby="fnameHelp"
                       placeholder="Enter name" value={this.state.name} onChange={this.onChangeName}/>
            </div>
            <div className="form-group">
                <label htmlFor="txtline1">Address: </label>
                <input type="text" className="form-control" id="txtline1" aria-describedby="fnameHelp"
                       placeholder="Enter address" value={this.state.line1} onChange={this.onChangeLine1}/>
            </div>

            <div className="form-group">
                <label htmlFor="exampleid">   Id : </label>
                <input type="number" className="form-control" id="exampleid" aria-describedby="emailid"
                       placeholder="Enter id" value={this.state.id} onChange={this.onChangeId}/>
            </div>

            <div className="form-group">
                <input type="submit" className="form-control"  onClick={this.saveCustomer}/>
            </div>

        </form>)
    }
}


